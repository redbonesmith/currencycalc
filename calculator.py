__author__ = 'Oleg.Koval'
import urllib2
from bs4 import BeautifulSoup
import autopep8

def table_tag_parser():
    content_url = "http://www.bank.gov.ua/control/en/curmetal/detail/currency?period=daily"
    soup = BeautifulSoup(urllib2.urlopen(content_url).read())
    table = soup.find('div', attrs={'class': 'content'})
    rows = table.findAll('tr')
    for tr in rows:
        cols = tr.findAll('td')
        if 'cell_c' in cols[0]['class']:
            # currency row
            digital_code, letter_code, units, name, rate = [item.text for item in cols]
            #print digital_code, letter_code, units, name, rate
            #listOfParams = (digital_code, letter_code, units, name, rate)

            textData = (digital_code, letter_code, name)
            numericalData = (units, rate)

            yield [textData,numericalData]

def text_numeric_data_converter():
        #convertor [tested]
        genericData = table_tag_parser()
        for textData,numericalData in genericData:
            cs_digital_code = str(textData[0])
            cs_letter_code = str(textData[1])
            cn_units = int(numericalData[0])
            cs_name = str(textData[2])
            cn_rate = float(numericalData[1])


            #my first perfect yield MADAFAKA
            yield cs_digital_code, cs_letter_code, cs_name, cn_rate, cn_units

class Representation(object):
    #generator obj created
    genericConvertedData = text_numeric_data_converter()
    #genericConvertedData converted to list
    list_of_converted_data = map(list, genericConvertedData)
    #print list_of_converted_data
    data_container = {}
    for i in list_of_converted_data:
        data_container[i[1]] = {
            'id': i[0],
            'short_name': i[1],
            'name': i[2],
            'rate': i[3],
            'units': i[4]
        }
