__author__ = 'Oleg.Koval'
from pymongo import MongoClient as ConnectorObject
from calculator import Representation as DataModel
import itertools

#database_client => collection
client = ConnectorObject('localhost', 27017)

#A single instance of MongoDB can support multiple independent databases. \n
#When working with PyMongo you access databases using attribute style access on MongoClient instances:

db = client.currencydb

#A collection is a group of documents stored in MongoDB, and can be thought \n
#of as roughly the equivalent of a table in a relational database. Getting a\n
#collection in PyMongo works the same as getting a database:

collection = db.money
#print(DataModel.data_container)

collection.drop()
some_post_id = collection.insert(DataModel.data_container)

iterator_object = collection.find()
print (iterator_object.__getitem__(0))